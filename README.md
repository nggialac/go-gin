# Golang Gin Framework - Video API (POC)

## Go Module Init

```bash
go mod init github.com/lacnguyen/go-gin
```

## Gin-Gonic library: github.com/gin-gonic/gin

## Run

```bash
go run main.go
```

# Swagger Documentation

## Install Swagger Library

```bash
go get -u github.com/swaggo/swag/cmd/swag
```

## Generate Swagger Documentation

```bash
swag init
```
